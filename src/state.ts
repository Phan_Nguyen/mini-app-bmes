import { atom, selector } from "recoil";
import { getUserInfo } from "zmp-sdk";

import { getStorage } from "zmp-sdk/apis";
import axios from 'axios';

const BASE_URL = `https://beta.bmes.vn/api/`;

export const userState = selector({
  key: "user",
  get: () =>
    getUserInfo({
      avatarType: "normal",
    }),
});

export const displayNameState = atom({
  key: "displayName",
  default: "",
});
