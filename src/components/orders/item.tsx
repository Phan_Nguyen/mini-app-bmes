import React, { FC } from "react";
import { Box, Text, Icon } from "zmp-ui";


export const OrderItemC: FC<{ order: any }> = ({ order, onClick }) => {
  return (
    
        <div className="flex py-3 mb-2 shadow-sm  rounded-md hover:bg-slate-100 cursor-pointer" onClick={onClick}>
          <div  className="pl-4">
          <Text size="xxSmall">
            # {order.code} <br/>
            {order.name} <br/>
            KL: {order.khoiluongdathang.toLocaleString()} <br/>
            Khu vực: {order.zone.name} <br/>
            Trạng thái: {order.trangthaiid.name} <br/>
            </Text>
          </div>
          <div>
        </div>
       </div>
  );
};
