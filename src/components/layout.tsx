import React, { FC } from "react";
import { Route, Routes } from "react-router";
import { Box } from "zmp-ui";
import HomePage from "pages/index";
import About from "pages/about";
import Form from "pages/form";
import User from "pages/user";
import Dashboard from "pages/dashboard";
import Order from "pages/orders/index";
import Login from "pages/login";
import OrderDetail from "pages/orders/order-detail/order-detail";
import OrderCreate from "pages/orders/order-create";
import { getSystemInfo } from "zmp-sdk";
import { ScrollRestoration } from "./scroll-restoration";
import  BottomNavigation from "./navigation";



export const Layout: FC = () => {

  return (
    <Box flex flexDirection="column" className="h-screen">
      {/* <ScrollRestoration /> */}
      <Box className="flex-1 flex flex-col overflow-hidden">
        <Routes>
        <Route path="/" element={<HomePage></HomePage>}></Route>
              <Route path="/about" element={<About></About>}></Route>
              <Route path="/form" element={<Form></Form>}></Route>
              <Route path="/user" element={<User></User>}></Route>
              <Route path="/dashboard" element={<Dashboard></Dashboard>}></Route>
              <Route path="/order" element={<Order></Order>}></Route>
              <Route path="/order-create" element={<OrderCreate></OrderCreate>}></Route>
              <Route path="/order-detail" element={<OrderDetail></OrderDetail>}></Route>
              {/* <Route path="/login" element={<Login></Login>}></Route> */}
        </Routes>
      </Box>
      {/* <BottomNavigation /> */}
    </Box>
  );
};
