import React, {FC, useState} from 'react';
import { Picker } from "zmp-ui";
import { setDataHeaders } from "utils";
import axios from 'axios';

export const  PickerComponent : FC = ({data, title, label, placeholder, setDataToSelect}) => {


  const formatObj = (obj) => {
    const newObj = {id: obj?.option?.value, name: obj?.option?.displayName};
    return newObj;    

  };

  const handleSelect = (value) => {    
    const newVal =  formatObj(value);
    console.log(newVal);
    
    
    setDataToSelect(newVal);
  }

  return (
    <>
    <Picker
            label={label}
            placeholder={placeholder}
            mask
            maskClosable
            title={title}
            action={{
              text: "Chọn",
              close: true
            }}
            data={[
              {
                options: Array.isArray(data) ? data.map((d, i) => ({
                  displayName: d.name,
                  value: d.id,
                  key: d.id
                })) : [],
                name: "option"
                
              } 
            ]}

            onChange={(value) => {handleSelect(value)}}
            
          />
          </>
  )
}
