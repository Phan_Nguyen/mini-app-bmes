import React, {FC, useEffect, useState, useCallback} from 'react';
import { Picker, Select } from "zmp-ui";
import { setDataHeaders } from "utils";
import axios from 'axios';
import {PickerComponent} from "./picker"


const { OtpGroup, Option } = Select;

export const  SelectComponent : FC = (
  {
    title, label, placeholder, url, rp = "", params = {page: 1, pageSize: -1}, setDataToParent
  }
) => {
  const [data, setData] = useState({});

  const loadDataAPI = useCallback(async() => {
    const headers =  setDataHeaders();   
      const response = await axios.get(url, {params: params, headers});             
      rp === "content" ? setData(response.data.content) : setData(response.data); 
  }
) 
  useEffect(() => {
    loadDataAPI();
  }, [])
  
  const handleDataChange = (value) => {
    setDataToParent({id: value});
  }


  return (
    <>
    <Select
     label={label}
     placeholder={placeholder}
     closeOnSelect={true}
     onChange={handleDataChange}
     >
      { Array.isArray(data) ? data.map((d) => (
      <Option key={d.id} value={d.id} title={d.name} />
      )) : []}
  </Select>
    </>
  );
}

// export const  SelectComponent : FC = (
//   {dataTemp, title, label, placeholder, url, rp = "", params = {page: 1, pageSize: -1}, setDataToParent}
// ) => {
//   const [data, setData] = useState({});
  
//   const loadDataAPI = async() => {

//     const headers =  setDataHeaders();
//     if (!dataTemp) {
//       const response = await axios.get(url, {params: params, headers});          

//       rp === "content" ? setData(response.data.content) : setData(response.data); 
//     } else {
//       setData(dataTemp);

//     }
    
       
//   }

//   const handleDataFromPicker = (value) => {        
//     setDataToParent(value);
//   }

//     useEffect(() => {
//       loadDataAPI();      
//     }, [])

//   return (
//     <PickerComponent
//             label={label}
//             placeholder={placeholder}
//             mask
//             maskClosable
//             title={title}
//             action={{
//               text: "Chọn",
//               close: true
//             }}
//             data={data}
//             setDataToSelect={handleDataFromPicker}
//           />
        
//   )
// }
