import React, { useState } from "react";
import { BottomNavigation, Icon, Page, useNavigate } from "zmp-ui";
import { MenuItem } from "model/menu";

const tabs: Record<string, MenuItem> = {
  "/": {
    label: "Trang chủ",
    icon: <Icon icon="zi-poll" />,
    activeIcon: <Icon icon="zi-poll-solid"/>,
  },
 
  "/about": {
    label: "Tạo đơn hàng",
    icon: <Icon icon="zi-add-story" />,
  },
};

export type TabKeys = keyof typeof tabs;

const BottomNavigationPage = (props) => {
  const [activeTab, setActiveTab] = useState<TabKeys>("/");
  const navigate = useNavigate();

  return (
    <BottomNavigation
      id="footer"
      activeKey={activeTab}
      onChange={(key: TabKeys) => setActiveTab(key)}
   
    >
      {Object.keys(tabs).map((path: TabKeys) => (
        <BottomNavigation.Item
          key={path}
          label={tabs[path].label}
          icon={tabs[path].icon}
          activeIcon={tabs[path].activeIcon}
          onClick={() => navigate(path)}
        />
      ))}
    </BottomNavigation>
  );
};

export default BottomNavigationPage;