import { setDataHeaders } from 'hook';
import { setDataToken } from 'hook';
import _ from "lodash";
import { useEffect, useRef, useState } from "react";
import { EventName, events, Payment } from "zmp-sdk";
import { useNavigate, useSnackbar } from "zmp-ui";


const setDataToken = () => {
    const value = localStorage.getItem('token');  
    return value;
}

export const setDataHeaders = () => {
    const value = {Authorization: `Bearer ${setDataToken()}`};
    return value;
};


export const notifyInsertSuccess = () => {
    const snackbar = useSnackbar();

    return () =>
      snackbar.openSnackbar({
        type: "success",
        text: "Thêm thành công",
      });
  }

  export const notifyUpdateSuccess = () => {
    const snackbar = useSnackbar();

    return () =>
      snackbar.openSnackbar({
        type: "success",
        text: "Cập nhật thành công",
      });
  }
  
