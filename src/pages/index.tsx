import React, { Suspense } from "react";
import { List, Page, Icon, useNavigate, Box } from "zmp-ui";
import UserCard from "components/user-card";
import Login from "pages/login";

const HomePage: React.FunctionComponent = () => {
  const navigate = useNavigate();
  return (
    <Page>
      <Box>
        <Login/>
      </Box>
    </Page>
  );
};

export default HomePage;
