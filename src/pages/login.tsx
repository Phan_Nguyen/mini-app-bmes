import React, {useEffect, useState} from "react";
import {
  Avatar,
  List,
  Text,
  Box,
  Page,
  Button,
  Icon,
  useNavigate,
  Input
} from "zmp-ui";
import { displayNameState, userState } from "state";
import { setStorage } from "zmp-sdk/apis";
import axios from 'axios';
import { getStorage } from "zmp-sdk/apis";

const URL = 'https://beta.bmes.vn/api/auth/token';

const LoginPage = () => {
    
    const [username, setUserName] = useState("bmes"); 
    const [password, setPassword] = useState("itbmes.vn123"); 

    const navigate = useNavigate();


    async function login() {    
        const formData = new FormData()
            formData.append('grant_type', 'password')
            formData.append('scope', 'read write')
            formData.append('client_id', 'concretemixer_mobileapp')
            formData.append('client_secret', 'cec@123')
            formData.append('username', username)
            formData.append('password', password)

            const response = await axios({
                method: 'post',
                url: 'api/oauth/token',
                data: formData,
                headers: {
                    "Authorization": 'Basic Y29uY3JldGVtaXhlcl9tb2JpbGVhcHA6Y2VjQDEyMw==',
                },
            });
            const dataResponse = response.data.access_token;     
            localStorage.setItem('token', dataResponse); 
            navigate("/order");
    }

 

  return (
    <Page className="section-container flex items-center justify-center">
        <div className="grow">
            <Input label="Tên đăng nhập" placeholder="Tên đăng nhập"
            onChange={(e)=> setUserName(e.target.value)} />
            <Input.Password label="Mật khẩu"
            onChange={(e)=> setPassword(e.target.value)} visibilityToggle/>
            <br/>

            <br/>
            <Button
                variant="primary"
                fullWidth
                onClick={login}
            >
                Đăng nhập
            </Button>
        </div>

    </Page>
  );
};

export default LoginPage;
