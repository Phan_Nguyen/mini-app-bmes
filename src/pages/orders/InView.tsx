import React, {forwardRef, useEffect} from 'react'

import { useInView } from "react-intersection-observer";

export const InView = ({ loadMore }) => {
    const [ref, inView] = useInView({
        triggerOnce: false, // Only trigger once when the element comes into view
      });

      useEffect(() => {
        if (inView) {
          loadMore();
        }
      }, [inView, loadMore]);

      return (
    <div ref={ref} style={{height: '100px'}}/>
  )
}

