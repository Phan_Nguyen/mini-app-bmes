import React, {FC, useState, useEffect} from 'react'
import { 
  Box,
  Text,
  DatePicker,
   Page, 
   Input, 
   Button, 
   Icon,
   Picker,
   useNavigate,
   useSnackbar,
   Switch,
   Select,
   Header
  } from "zmp-ui";
  import axios from 'axios';
  import { setDataHeaders, notifyInsertSuccess, notifyUpdateSuccess } from "utils";
  import { SelectComponent } from "components/select";

  const { OtpGroup, Option } = Select;

export const OrderCreate: FC = () => {
    const [date, setdate] = useState(null);
    const [zone, setzone] = useState(new Date());
    const [khachhangid, setkhanchhangid] = useState({});
    const [hopdongid, sethopdongid] = useState({});
    const [code, setcode] = useState("");
    const [trangthaiid, settrangthaiid] = useState({});
    const [name, setname] = useState("");
    const [khoiluongdathang, setkhoiluongdathang] = useState("");
    const [note, setnote] = useState("");
    const [isactive, setisactive] = useState("");

    const [khachHangList, setKhachHangList] = useState([]);
    const [hdList, sethdList] = useState([]);
    const [ttList, setttList] = useState([]);

    const navigate = useNavigate();
    
    const headers =  setDataHeaders();
    const notifyInsert = notifyInsertSuccess();

    const genCode = async () => {
      const params = { page: 1, pageSize: 100, tablename: 't_tron_hopdongtong'};
      const response = await axios.get("api/t_code_generated/find", {params: params, headers});  
      console.log(response);
                 
      setcode(response.data);
    }

    const loadKhachhang = async () => {
      const params = { page: 1, pageSize: -1};
      const response = await axios.get("api/t_master_khachhang/fuzzySearch/find", {params: params, headers});             
      setKhachHangList(response.data);
    }

    const loadTrangthai = async () => {
      const params = { page: 1, pageSize: -1};
      const response = await axios.get("api/t_tron_trangthai/find", {params: params, headers});             
      setttList(response.data);
      settrangthaiid({id: 0});
    }

    const onChangeZone = (zone) => {
      setzone(zone);
    }

    const onChangeKhachhang = async (kh) => {      
      const params = {
        page: 0,
        size: 1000,
        query: `khachhangid.id==${kh}`
      }
      const response = await axios.get("api/contract/search", {params: params, headers});                   
      sethdList(response.data.content);
      setkhanchhangid({id: kh});
    }

    const onChangeHopDong = (id) => {
      console.log(hopdoingid);
      
      sethopdongid({id: id});
    }

    const onChangeTrangThai = (tt) => {
      settrangthaiid({id: parseInt(tt, 10)});
    }

    const submit = async() => {
     
      const body = {
        code,
        name,
        date: date.toString(),
        zone,
        khachhangid,
        trangthaiid,
        khoiluongdathang,
        note,
        isactive,
        hopdongid,
      }
      console.log(body);

      // const response = await axios.post("api/t_tron_hopdongtong", body, {headers});  
      // navigate('/order');
      // notifyInsert();

    }



    useEffect(() => {
      // genCode();
      loadKhachhang();    
      loadTrangthai();
    },[])
    

  
  return (
    <Page className="flex flex-col" >
      <Header title="Thêm mới" />
      <Box className="section-container">
      <form>
        <Box mt={3}>
        <Input 
          label="Mã" 
          value={code}
          onChange={(e) => setcode(e.target.value)} 
          disabled
          />
      </Box>

      <Box mt={3}>
        <Input 
          label="Tên" 
          value={name}
          onChange={(e) => setname(e.target.value)} 
          />
      </Box>

      <Box mt={3}>
      <DatePicker
          label='Ngày'
          title='Thời gian đặt hàng'
          dateFormat='dd/mm/yyyy'
          mask
          maskClosable
          dateFormat="dd/mm/yyyy"
          title="DatePicker"
          value={date}
          onChange={setdate}
      />   
      </Box>

      <Box mt={3}>
        <SelectComponent 
          url="api/zone/find"
          rp="content"
          title="Chọn khu vực"
          label="Khu vực"
          placeholder="Chọn khu vực"
          params={{page: 0, size: 64}}
          setDataToParent={onChangeZone}
          />
      </Box>

      <Box mt={3}>
        <Select
            title="Chọn khách hàng"
            label="Khách hàng"
            placeholder="Chọn khách hàng"
            closeOnSelect={true}
            onChange={onChangeKhachhang}
            >
              { 
                khachHangList.map((d) => (
                  <Option key={d.id} value={d.id} title={d.name} />
                ))
              }
          </Select>
      </Box>

      <Box mt={3}>
      <Select
            title="Chọn hợp đồng"
            label="Hợp đồng"
            placeholder="Chọn hợp đồng"
            closeOnSelect={true}
            onChange={onChangeHopDong}
            >
               { 
                hdList.map((d) => (
                  <Option key={d.id} value={d.id} title={d.name} />
                ))
              }
          </Select>
      </Box>

      <Box mt={3}>
      <Select
            title="Chọn trạng thái"
            label="Trạng thái"
            placeholder="Chọn trạng thái"
            closeOnSelect={true}
            defaultValue="0"
            onChange={onChangeTrangThai}
            >
              { 
                ttList.map((d) => (
                  <Option key={d.id} value={d.id.toString()} title={d.name} />
                ))
              }
              
          </Select>
      </Box>

      <Box mt={3}>
        <Input 
          type="number"
          label="Khối lượng đặt hàng" 
          value={khoiluongdathang}
          onChange={(e) => setkhoiluongdathang(e.target.value)} 
          />
      </Box>
      
      <Box mt={3}>
        <Input 
          label="Ghi chú" 
          value={note}
          onChange={(e) => setnote(e.target.value)} 
          />
      </Box>

      </form>

      <div className="py-5 flex justify-between">
        <Button
             variant="secondary"
             onClick={() => navigate("/order")}
          >
            Quay lại
        </Button>

        <Button
             variant="primary"
             onClick={submit}
          >
            Lưu
        </Button>
      </div>
        </Box>
    </Page>
  )
}

export default OrderCreate;