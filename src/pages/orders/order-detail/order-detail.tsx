import React, {FC, useState, useEffect, Suspense} from 'react'
import { 
  Box,
  Text,
  DatePicker,
   Page, 
   Input, 
   Button, 
   Icon,
   Picker,
   useNavigate,
   useSnackbar,
   Switch,
   Select,
   Header
  } from "zmp-ui";
  import axios from 'axios';
  import { useLocation } from "react-router";
  import { setDataHeaders, notifyInsertSuccess, notifyUpdateSuccess } from "utils";
  import { SelectComponent } from "components/select";
  import qs from 'qs';

  const { OtpGroup, Option } = Select;


  export const OrderDetailItemC: FC = ({ data }) => {
   
    return (
          <div className="flex py-3 mb-2 shadow-sm  rounded-md hover:bg-slate-100 cursor-pointer">
            <div  className="pl-4">
            <Text size="xxSmall">
              # {data.code} <br/>
              {data.maccongtyid.name} <br/>
              {data.congtruongid.name} <br/>
              KL: {data.khoiluongdathang} <br/>
              {data.trangthaiid.name} <br/>
              </Text>
            </div>
            <div>
          </div>
         </div>
    );
  };


  export const OrderDetailItem: FC = () => {
    const [dataDetail, setDataDetail] = useState([]);
    const headers =  setDataHeaders();
    const location =  useLocation();

    const loadData = async() => {      
      const params = {
        page: 1,
        pageSize: 100,
        hopdongtongid: JSON.stringify({id: location.state})
      }
      const response = await axios.get(`api/t_tron_hopdongchitiet/find`, {params, headers});
      setDataDetail(response.data)
    }
  
    useEffect(() => {
      loadData();
    }, [])
    
      return (
          <div className="" >
              {dataDetail.map((detail) => (
                <OrderDetailItemC key={detail.id} data={detail}/>
              ))}         
          </div>
        );
      };

export const OrderDetail: FC = () => {

    const navigate = useNavigate();
    return (
      <Page>
        <Header title="Danh sách đơn hàng chi tiết"/>
        <div className="flex m-4 p-3 justify-center rounded-full border-dashed border-2 border-slate-300 cursor-pointer active:bg-slate-300"
             onClick={() => navigate("/order-create")}>
          <Text className="me-2 align-middle"> Thêm mới </Text>
          <Icon icon="zi-plus"/>
        </div>
        <div className="section-container">
         <OrderDetailItem />
          </div>
      </Page>
    );
}

export default OrderDetail;