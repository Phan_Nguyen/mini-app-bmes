import React from "react";
import { FC } from "react";
import { Box, Input, useNavigate, Button, Text, Icon } from "zmp-ui";

export const Inquiry: FC = () => {
  const navigate = useNavigate();
  return (
    <Box py={4}>
      <Input.Search
        placeholder="Tìm kiếm đơn hàng"
      /> 
    </Box>
  );
};
