import React, { Suspense, useState, useEffect, useCallback, useNa } from "react";
import { Box, Page, Text, Button, useNavigate, Icon } from "zmp-ui";
import { Inquiry } from "./inquiry";
import { OrderList } from "./order-list";
import {InView} from './InView'
import { getStorage } from "zmp-sdk/apis";
import axios from 'axios';
 

const OrderPage: React.FunctionComponent = () => {
  const [orderList, setOrderList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [page, setPage] = useState(1);

  const navigate = useNavigate();
  
  const getAPIData =  useCallback(async (page) => {
      try {
        setIsLoading(true);

       const token = localStorage.getItem('token');
        
         const params = {
          page: page,
          pageSize: 10
        }
        const headers = {Authorization: `Bearer ${token}`};
          
        const response = await axios.get('api/t_tron_hopdongtong/fuzzySearch/find', {params: params, headers});
  
        setOrderList(prevItems => [...prevItems, ...response.data]);
        setPage(pagePrev => page + 1);
        
      } finally {
        setIsLoading(false);
      }
    
  },)

  useEffect(() => {
    getAPIData(1);
  }, []);

  const loadMore = useCallback(() => {    
    if(isLoading) {
      return;
    }
    getAPIData(page);
  }, [page, isLoading])

  return (
    <Page className="section-container relative flex-1 flex flex-col">
    
      <Box className="flex-1 overflow-auto ">
        <Inquiry />
        <div className="flex">
        <Text size="large" className=" pb-4 pe-3">Danh sách đơn hàng</Text>
        <Box className="cursor-pointer">
            <Icon icon="zi-plus-circle" onClick={() => navigate("/order-create")}/>
        </Box>
        </div>
              
        <OrderList data={orderList} />
        <InView loadMore={loadMore}/>
      </Box>
    </Page>
  );
};

export default OrderPage;
