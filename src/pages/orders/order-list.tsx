import React, {  Suspense, FC, useEffect, useState, useRef } from "react";
import { Box,  Button,  Text, useNavigate } from "zmp-ui";
import {OrderItemC} from "components/orders/item";

export const OrderItem: FC = (
  {data}: {data: any[]}
) => {

  const navigate = useNavigate();

  return (
      <div className="">
          {data.map((order) => (
            <OrderItemC key={order.id} order={order} onClick={() => navigate(`/order-detail`, {state: order.id})}/>
          ))}         
      </div>
    );
  };




export const OrderList: React.FunctionComponent = ({data}: {data: any[]}) => {

    return (
      <Suspense className=""> 
         <OrderItem data={data} />
      </Suspense>
    );
  
  };
  