import React , { Suspense, FC } from "react";
import { Page, Button, Box, Text, Icon } from "zmp-ui";
import BottomNavigation from "components/bottomnavigation";

export const TramBox: FC = () => {

  return (
    <Box className="section-container">
    <Box mt={1}>
      <Button size="large">Button</Button>
    </Box>
    <Box mt={10}>
      <Button size="large">Button</Button>
    </Box>
  </Box>
  );
};

export const TramItemBox: FC = () => {

  return (
    <Box className="section-container">
    <Box mt={1}>
    <div className="flex justify-center">
    <p className="text-white text-lg font-bold ">1000</p>
    </div>
    </Box>
  
  </Box>
  );
};

const Dashboard: React.FunctionComponent = (props) => {

  return (
    <Suspense className="overflow-auto"> 
      <TramBox />
      <TramItemBox />
    </Suspense>
  );

};

export default Dashboard;