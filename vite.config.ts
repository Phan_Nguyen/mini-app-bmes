import { defineConfig } from "vite";
import tsconfigPaths from "vite-tsconfig-paths";
import react from "@vitejs/plugin-react";

// https://vitejs.dev/config/
export default () => {
  return defineConfig({
    root: "./src",
    base: "",
    plugins: [react(), tsconfigPaths()],
    server: {
      port: 3000,
      proxy: {
          '/api': {
              target: 'https://beta.bmes.vn/api',
              changeOrigin: true,
              secure: false,
              rewrite(path) {
                  return path.replace(/^\/api/, '')
              },
          }
      }
  },
  });
};
